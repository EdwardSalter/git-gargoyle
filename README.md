

## Dev Environment Setup
### Missing openssl header error:
- Download and install OpenSSL for windows manually. It should be v1 from https://slproweb.com/products/Win32OpenSSL.html
- Set the _openssl_dir_ config in npm after installing: `npm config set openssl_dir "<path_to_top_level_ssl_folder>" --global`.


### Errors regarding node-gyp of not found files in Visual Studio
- Open Visual Studio Installer
  - Modify the installation 
  - Add Desktop development with C++ if not already checked.
  - Ensure MSVC v141 - VS 2017 C++ build tools is selected in the right hand panel.
  - Modify the installation.
- Ensure _msbuild_path_ config is correct in npm: `npm config set msbuild_path "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\MSBuild.exe" --global`


### vcvarsall.bat not found
Ensure that _vcvarsall_path_ is set in npm: `npm config set vcvarsall_path "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvarsall.bat" --global`
