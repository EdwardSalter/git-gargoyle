const fs = require('fs-extra');
const path = require('path');

function getNodeGitDependencies() {
  const nodegitPath = path.resolve(__dirname, 'node_modules/nodegit');
  const packageJson = fs.readJsonSync(
    path.resolve(nodegitPath, 'package.json')
  );
  return Object.keys(packageJson.dependencies);
}

function copyNodeDependency(targetFolder, dependency) {
  const source = path.resolve(__dirname, `node_modules/${dependency}`);
  const dest = path.resolve(targetFolder, `node_modules/${dependency}`);
  return fs.copy(source, dest);
}

module.exports = {
  packagerConfig: {
    // tmpdir: false,
    afterCopy: [
      async (buildPath, electronVersion, platform, arch, callback) => {
        const copy = copyNodeDependency.bind(null, buildPath);

        const dependencies = getNodeGitDependencies();
        dependencies.push('nodegit');
        try {
          await Promise.all(dependencies.map(copy));
          callback();
        } catch (e) {
          callback(e);
        }
      },
    ],
  },
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {
        name: 'gitkraken_clone',
      },
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin'],
    },
    {
      name: '@electron-forge/maker-deb',
      config: {},
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {},
    },
  ],
  plugins: [
    [
      '@electron-forge/plugin-webpack',
      {
        port: 7000,
        mainConfig: './webpack.main.config.js',
        renderer: {
          config: './webpack.renderer.config.js',
          entryPoints: [
            {
              html: './src/index.html',
              js: './src/renderer/index.ts',
              name: 'main_window',
            },
          ],
        },
      },
    ],
  ],
};
