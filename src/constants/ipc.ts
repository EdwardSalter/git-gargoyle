export enum FromRenderer {
  SELECT_FOLDER_START = 'SELECT_FOLDER_START',
  GIT_LOG_START = 'GIT_LOG_START',
  GIT_BRANCH_START = 'GIT_BRANCH_START',
  SHOW_CONTEXT_MENU = 'SHOW_CONTEXT_MENU',
}
export enum FromMain {
  SELECT_FOLDER_FINISH = 'SELECT_FOLDER_FINISH',
  'GIT_LOG_FINISH' = 'GIT_LOG_FINISH',
  'GIT_REF_DELETE_FINISH' = 'GIT_REF_DELETE_FINISH',
}
