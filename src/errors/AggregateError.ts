class AggregateError<T extends Error> extends Error {
  innerErrors: T[];

  constructor(innerErrors: T[]) {
    super(
      'One of more errors have occurred. Look at the `innerErrors` property for more information.'
    );

    this.innerErrors = innerErrors;
  }
}

export default AggregateError;
