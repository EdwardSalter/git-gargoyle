import { Error as GitError } from 'nodegit';

const map = {
  '0': 'OK',
  '-1': 'ERROR',
  '-3': 'ENOTFOUND',
  '-4': 'EEXISTS',
  '-5': 'EAMBIGUOUS',
  '-6': 'EBUFS',
  '-7': 'EUSER',
  '-8': 'EBAREREPO',
  '-9': 'EUNBORNBRANCH',
  '-10': 'EUNMERGED',
  '-11': 'ENONFASTFORWARD',
  '-12': 'EINVALIDSPEC',
  '-13': 'ECONFLICT',
  '-14': 'ELOCKED',
  '-15': 'EMODIFIED',
  '-16': 'EAUTH',
  '-17': 'ECERTIFICATE',
  '-18': 'EAPPLIED',
  '-19': 'EPEEL',
  '-20': 'EEOF',
  '-21': 'EINVALID',
  '-22': 'EUNCOMMITTED',
  '-23': 'EDIRECTORY',
  '-30': 'PASSTHROUGH',
  '-31': 'ITEROVER',
};

export default class GitOperationError extends Error {
  code: GitError.CODE;

  constructor(code: GitError.CODE, message?: string) {
    super(message || `Git has encountered an error: ${map[code]}`);
    this.code = code;
  }
}
