import { FromMain, FromRenderer } from './constants/ipc';
import { ipcRenderer } from 'electron';
import { Reference } from './main/git';
import { ContextMenu } from './constants/ContextMenu';

function invoke(
  channel: FromRenderer.GIT_BRANCH_START,
  path: string
): Promise<Reference[]>;
function invoke(channel: FromRenderer, ...args: unknown[]): Promise<any> {
  if (FromRenderer[channel]) {
    return ipcRenderer.invoke(channel, ...args);
  }
  return Promise.resolve('Attempting to invoke on an invalid channel');
}

function send(channel: FromRenderer.SELECT_FOLDER_START): void;
function send(
  channel: FromRenderer.SHOW_CONTEXT_MENU,
  menu: ContextMenu.ReferencePanel,
  repoPath: string,
  references: string[]
): void;
function send(channel: FromRenderer, ...args: unknown[]): void {
  // whitelist channels
  if (FromRenderer[channel]) {
    ipcRenderer.send(channel, ...args);
  }
}

function on(channel: FromMain, func: (...args: unknown[]) => void): void {
  if (FromMain[channel]) {
    // Deliberately strip event as it includes `sender`
    ipcRenderer.on(channel, (event, ...args) => func(...args));
  }
}

export default {
  send,
  invoke,
  on,

  off(channel: FromMain, listener: (...args: unknown[]) => void): void {
    if (FromMain[channel]) {
      ipcRenderer.off(channel, listener);
    }
  },
};
