import {
  BrowserWindow,
  ContextMenuParams,
  ipcMain,
  IpcMainEvent,
  Event,
  Menu,
  MenuItem,
  MenuItemConstructorOptions,
} from 'electron';
import { FromMain, FromRenderer } from '../constants/ipc';
import { ContextMenu } from '../constants/ContextMenu';
import isDev from 'electron-is-dev';
import { deleteReferences, getReferences } from './git';

function init(win: BrowserWindow): void {
  let prepend: Array<MenuItem | MenuItemConstructorOptions> = [];

  function onContextMenu(event: Event, props: ContextMenuParams) {
    console.log('Global context menu');

    const DEFAULT_MENU_ITEMS: Array<MenuItem | MenuItemConstructorOptions> = [
      prepend.length > 0 &&
        isDev && {
          type: 'separator' as MenuItemConstructorOptions['type'],
        },
      isDev && {
        label: 'I&nspect Element',
        click: () => {
          win.webContents.inspectElement(props.x, props.y);

          if (win.webContents.isDevToolsOpened()) {
            win.webContents.devToolsWebContents.focus();
          }
        },
      },
    ].filter(Boolean);

    const menu = Menu.buildFromTemplate(prepend.concat(DEFAULT_MENU_ITEMS));
    menu.popup();
    menu.once('menu-will-close', () => {
      prepend = [];
    });
  }

  win.webContents.on('context-menu', onContextMenu);

  ipcMain.on(
    FromRenderer.SHOW_CONTEXT_MENU,
    (event: IpcMainEvent, menuId: ContextMenu, ...args: any) => {
      console.log('Building context menu:', menuId, args);

      const send = event.sender.send.bind(event.sender);
      prepend = menuMap[menuId](send, ...args);
    }
  );
}

const menuMap: Record<
  ContextMenu,
  (
    send: (command: string, ...args: any) => void,
    ...args: any
  ) => Array<MenuItem | MenuItemConstructorOptions>
> = {
  [ContextMenu.ReferencePanel]: (
    send: (command: string, ...args: any) => void,
    repoPath: string,
    refs: string[]
  ): Array<MenuItem | MenuItemConstructorOptions> => {
    return [
      {
        label:
          refs.length === 1
            ? `Delete ${refs[0]}`
            : `Delete ${refs.length} references`, // TODO: Could send context from renderer (i.e. 'branch' | 'remote' | 'tag' and adjust string
        // TODO: Format number
        click: () => {
          deleteReferences(repoPath, refs)
            .catch((e) => {
              console.error('An error occurred whilst deleting references:', e);
              send(FromMain.GIT_REF_DELETE_FINISH, e, refs);
            })
            .then(() => {
              send(FromMain.GIT_REF_DELETE_FINISH, null, refs);
            });
        },
      },
    ];
  },
};

export default init;
