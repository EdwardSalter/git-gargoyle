import { Repository, Branch as GitBranch, Error as GitError, Reference as GitReference } from 'nodegit';
import { castArray, uniqBy } from 'lodash-es';
import GitOperationError from '../errors/GitOperationError';
import AggregateError from '../errors/AggregateError';

const repos = new Map<string, Repository>();

async function getRepo(repoPath: string): Promise<Repository> {
  let repo = repos.get(repoPath);
  if (repo) return repo;

  repo = await Repository.open(repoPath);
  repos.set(repoPath, repo);
  return repo;
}

type ReferenceType = 'remote' | 'tag' | 'branch';

export interface Reference {
  name: string;
  upstream?: string;
  type: ReferenceType;
}

export async function getReferences(repoPath: string): Promise<Reference[]> {
  const repo = await getRepo(repoPath);
  const refs = await repo.getReferences();

  const branchesAndTags = uniqBy(
    refs.filter((ref) => ref.isRemote() || ref.isBranch() || ref.isTag()),
    (ref) => ref.shorthand()
  );

  return Promise.all(
    branchesAndTags.map(async (branch) => {
      let upstream: GitReference | undefined = undefined;
      if (!branch.isRemote()) {
        try {
          upstream = await GitBranch.upstream(branch);
        } catch (e) {
          console.error(e);
        }
      }

      return {
        name: branch.shorthand(),
        upstream: upstream?.shorthand(),
        type: (branch.isTag()
          ? 'tag'
          : branch.isBranch()
          ? 'branch'
          : 'remote') as ReferenceType,
      };
    })
  );
}

/**
 *
 * @param repoPath
 * @param refName
 * @throws {GitOperationError} if the command failed
 */
export async function deleteReference(
  repoPath: string,
  refName: string
): Promise<void> {
  const repo = await getRepo(repoPath);
  const ref = await repo.getReference(refName);
  const code = ref.delete();
  if (code !== GitError.CODE.OK) {
    throw new GitOperationError(
      code,
      `Git has encountered an error whilst deleting branch '${refName}'`
    );
  }
}

/**
 *
 * @param repoPath
 * @param refs
 * @throws {AggregateError} An error containing a list of several errors that may have occurred whilst deleting each branch
 */
export async function deleteReferences(
  repoPath: string,
  refs: string | string[]
): Promise<void> {
  refs = castArray(refs);

  const errors: Error[] = [];
  const responses = refs.map(async (ref) => {
    let error: Error | null = null;
    try {
      await deleteReference(repoPath, ref);
    } catch (e) {
      error = e;
      errors.push(error);
    }
    return [ref, error];
  });

  await Promise.all(responses);
  if (errors.length > 0) {
    throw new AggregateError(errors);
  }
}
