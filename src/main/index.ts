import { app, BrowserWindow, dialog, ipcMain, IpcMainEvent } from 'electron';
import installExtension, {
  REACT_DEVELOPER_TOOLS,
} from 'electron-devtools-installer';
import isDev from 'electron-is-dev';
import path from 'path';
import { FromRenderer, FromMain } from '../constants/ipc';
import { getReferences } from './git';
import initContextMenu from './contextMenu';

declare const MAIN_WINDOW_WEBPACK_ENTRY: any;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  // eslint-disable-line global-require
  app.quit();
}

let mainWindow: BrowserWindow | undefined;
const createWindow = (): void => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: false,
      enableRemoteModule: false,
      contextIsolation: true,
    },
  });

  // and load the index.html of the app.
  console.log('Loading main app:', MAIN_WINDOW_WEBPACK_ENTRY);
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', onReady);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

function setSecurityPolicy() {
  // session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
  //   callback({
  //     responseHeaders: {
  //       ...details.responseHeaders,
  //       "Content-Security-Policy": ["script-src 'self' devtools://devtools"],
  //     },
  //   });
  // });
}

function onReady() {
  setSecurityPolicy();
  createWindow();
  installExtensions();
  initContextMenu(mainWindow);
}

function installExtensions() {
  installExtension(REACT_DEVELOPER_TOOLS)
    .then((name) => {
      console.log(`Added Extension:  ${name}`);
      // Open the DevTools.

      mainWindow.webContents.on('did-frame-finish-load', () => {
        if (isDev) {
          mainWindow.webContents.openDevTools();
          mainWindow.webContents.on('devtools-opened', () => {
            mainWindow.focus();
          });
        }
      });
    })
    .catch((err) => console.log('An error occurred: ', err));
}

ipcMain.on(FromRenderer.SELECT_FOLDER_START, async (event, arg) => {
  const result = await dialog.showOpenDialog(mainWindow, {
    properties: ['openDirectory'],
  });

  event.reply(
    FromMain.SELECT_FOLDER_FINISH,
    result.canceled ? null : result.filePaths[0]
  );
});

ipcMain.handle(
  FromRenderer.GIT_BRANCH_START,
  (event: IpcMainEvent, path: string, ...args: unknown[]) => {
    return getReferences(path);
  }
);
