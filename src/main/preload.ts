import { contextBridge } from 'electron';
import api from '../ipcApi';

contextBridge.exposeInMainWorld('api', api);

declare global {
  interface Window {
    api: typeof api;
  }
}
