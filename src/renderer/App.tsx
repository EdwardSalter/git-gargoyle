import React, { ReactElement, StrictMode } from 'react';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { CssBaseline, ThemeProvider } from '@mui/material';
import NoRepoOpenMessage from './NoRepoOpenMessage';
import useFolderChoiceDialog from './useFolderChoiceDialog';
import GitRepo from './GitRepo';
import theme from './theme';

const App = (): ReactElement => {
  const [repoPath, triggerFolderChoiceDialog] = useFolderChoiceDialog();

  const content = !repoPath ? (
    <NoRepoOpenMessage onOpen={triggerFolderChoiceDialog} />
  ) : (
    <GitRepo path={repoPath} />
  );

  return (
    <StrictMode>
      <ThemeProvider theme={theme}>
        <CssBaseline>{content}</CssBaseline>
      </ThemeProvider>
    </StrictMode>
  );
};

export default App;
