import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import ReferenceList from './ReferenceList';
import useReferences from './hooks/useReferences';
import RepositoryContext from './context/repository';
import { FromMain } from '../constants/ipc';
import Toast from './components/Toast';

interface GitRepoProps {
  path: string;
}

const GitRepo = ({ path }: GitRepoProps): ReactElement => {
  const [references, reloadReferences] = useReferences(path);

  // TODO: Triggering another delete needs to reset this.
  // TODO: Success/error messages can be cleaned up as a hook/component?
  const [deleteSuccessSnack, setDeleteSuccessSnack] = useState(false);
  const [deleteFailedSnack, setDeleteFailedSnack] = useState(false);
  const context = useMemo(
    () => ({
      path,
    }),
    [path]
  );

  const onRefDelete = useCallback(
    (e: Error | null) => {
      if (e) {
        setDeleteFailedSnack(true);
      } else {
        setDeleteSuccessSnack(true);
      }
      return reloadReferences();
    },
    [reloadReferences]
  );

  useEffect(() => {
    window.api.on(FromMain.GIT_REF_DELETE_FINISH, onRefDelete);
    return () => {
      window.api.off(FromMain.GIT_REF_DELETE_FINISH, onRefDelete);
    };
  }, [onRefDelete]);

  return (
    <RepositoryContext.Provider value={context}>
      <ReferenceList
        title="Local"
        icon="laptop"
        references={references.localBranches}
      />
      <ReferenceList
        title="Remote"
        icon="cloud"
        references={references.remoteBranches}
      />
      <ReferenceList
        title="Tags"
        icon="tags"
        listIcon="tag"
        references={references.tags}
      />

      <Toast
        open={deleteSuccessSnack}
        severity="success"
        onClose={() => {
          setDeleteSuccessSnack(false);
        }}
      >
        Successfully deleted
        {/* TODO: Add UNDO functionality using reflog? */}
      </Toast>
      <Toast
        open={deleteFailedSnack}
        severity="error"
        onClose={() => {
          setDeleteFailedSnack(false);
        }}
      >
        Failed to delete branches
      </Toast>
    </RepositoryContext.Provider>
  );
};

export default GitRepo;
