import { library } from '@fortawesome/fontawesome-svg-core';
import { styled } from '@mui/material/styles';
import {
  faLaptop,
  faCodeBranch,
  faCloud,
  faTags,
  faTag,
} from '@fortawesome/free-solid-svg-icons';
import {
  FontAwesomeIcon,
  FontAwesomeIconProps,
} from '@fortawesome/react-fontawesome';
import React, { FunctionComponent } from 'react';
import { omitProps } from './hocs/omitProps';

type StyleProps = Pick<IconProps, 'margin' | 'marginRight' | 'marginLeft'>;
const StyledFontAwesomeIcon = styled(
  omitProps(['margin', 'marginLeft', 'marginRight'])(FontAwesomeIcon)
)<StyleProps>((props) => ({
  margin: props.margin,
  marginLeft:
    props.marginLeft === true ? '1em' : (props.marginLeft as string | number),
  marginRight:
    props.marginRight === true ? '1em' : (props.marginRight as string | number),
}));

library.add(faLaptop, faCodeBranch, faCloud, faTags, faTag);
export type SupportedIcon = 'laptop' | 'code-branch' | 'cloud' | 'tags' | 'tag';

export type IconProps = Omit<FontAwesomeIconProps, 'icon'> & {
  name: SupportedIcon;
  margin?: string | number;
  marginLeft?: true | string;
  marginRight?: true | string;
};

const Icon: FunctionComponent<IconProps> = (props) => {
  const { name, ...iconProps } = props;

  return <StyledFontAwesomeIcon icon={name} {...iconProps} />;
};

export default Icon;
