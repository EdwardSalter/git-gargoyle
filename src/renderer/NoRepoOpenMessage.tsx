import React, { FunctionComponent } from 'react';
import { Button, Stack } from '@mui/material';

interface NoRepoOpenMessageProps {
  onOpen: () => void;
}

const NoRepoOpenMessage: FunctionComponent<NoRepoOpenMessageProps> = ({
  onOpen,
}) => {
  return (
    <Stack alignItems="center" justifyContent="center" height="100vh">
      Open a Git repository to get started
      <Button onClick={onOpen} sx={{ mt: 2 }}>
        Open Repository
      </Button>
    </Stack>
  );
};

export default NoRepoOpenMessage;
