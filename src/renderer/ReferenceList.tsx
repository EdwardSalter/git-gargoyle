import React, { ReactElement, useCallback, useState } from 'react';
import { Reference } from '../main/git';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
  Typography,
} from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Icon, { SupportedIcon } from './Icon';
import { FromRenderer } from '../constants/ipc';
import { ContextMenu } from '../constants/ContextMenu';
import useRepository from './hooks/useRepository';

const VerticallyCenteredText = styled(Typography)({
  display: 'flex',
  alignItems: 'center',
});

const FullWidthList = styled(List)({
  width: '100%',
});

interface ReferenceListProps {
  title: string;
  icon: SupportedIcon;
  listIcon?: SupportedIcon;
  references: Array<Reference>;
}

const ReferenceList = ({
  references,
  icon,
  listIcon = 'code-branch',
  title,
}: ReferenceListProps): ReactElement => {
  const [selection, setSelection] = useState<Record<string, boolean>>({});
  const [lastClickedIndex, setLastClickedIndex] = useState<number>();
  const { path } = useRepository();

  const toggleSelection = useCallback(
    (
      ref: Reference,
      clickedIndex: number,
      hasShiftKey: boolean,
      hasCtrlKey: boolean
    ) => {
      setSelection((oldState) => {
        const refName = ref.name;

        if (hasShiftKey) {
          const minIndex = Math.min(lastClickedIndex, clickedIndex);
          const maxIndex = Math.max(lastClickedIndex, clickedIndex);
          const refsToAddToSelection = references.slice(minIndex, maxIndex + 1);
          return refsToAddToSelection.reduce(
            (cur, ref) => {
              return {
                ...cur,
                [ref.name]: true,
              };
            },
            { ...oldState }
          );
        }

        if (hasCtrlKey) {
          return {
            ...oldState,
            [refName]: !oldState[refName],
          };
        }

        return { [refName]: true };
      });
      setLastClickedIndex(clickedIndex);
    },
    [references, lastClickedIndex]
  );

  const hasReferences = references.length > 0;

  return (
    <Accordion square>
      <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
        <VerticallyCenteredText>
          <Icon name={icon} marginRight />
          &nbsp;{title}
        </VerticallyCenteredText>
      </AccordionSummary>
      <AccordionDetails>
        {hasReferences && (
          <FullWidthList disablePadding>
            {references.map((ref, index) => (
              <ListItemButton
                sx={{ userSelect: 'none' }}
                key={ref.name}
                selected={!!selection[ref.name]}
                disableRipple
                onClick={(e) => {
                  toggleSelection(ref, index, e.shiftKey, e.ctrlKey);
                }}
                onContextMenu={() => {
                  // e.preventDefault();
                  const selectedRefs: string[] = selection[ref.name]
                    ? Object.entries(selection)
                        .filter(([, v]) => v)
                        .map(([k]) => k)
                    : [ref.name];

                  window.api.send(
                    FromRenderer.SHOW_CONTEXT_MENU,
                    ContextMenu.ReferencePanel,
                    path,
                    selectedRefs
                  );
                }}
              >
                <ListItemIcon>
                  <FontAwesomeIcon icon={listIcon} />
                </ListItemIcon>
                <ListItemText>{ref.name}</ListItemText>
              </ListItemButton>
            ))}
          </FullWidthList>
        )}
        {!hasReferences && (
          <Typography align="center" sx={{ m: 4 }}>
            No references
          </Typography>
        )}
      </AccordionDetails>
    </Accordion>
  );
};

export default ReferenceList;
