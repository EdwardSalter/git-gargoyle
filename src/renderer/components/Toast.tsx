import React, { PropsWithChildren, ReactElement } from 'react';
import { Alert, AlertProps } from '@mui/lab';
import { Snackbar, SnackbarProps } from '@mui/material';

type ToastProps = PropsWithChildren<
  Omit<SnackbarProps, 'children'> & Pick<AlertProps, 'severity'>
>;

const Toast = ({
  severity,
  autoHideDuration,
  children,
  ...snackbarProps
}: ToastProps): ReactElement => {
  if (autoHideDuration == null)
    autoHideDuration = severity === 'error' ? undefined : 6000;

  return (
    <Snackbar
      autoHideDuration={autoHideDuration}
      anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      {...snackbarProps}
    >
      <Alert severity={severity} variant="filled">
        {children}
      </Alert>
    </Snackbar>
  );
};

export default Toast;
