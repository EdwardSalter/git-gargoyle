import { createContext } from 'react';

export interface RepositoryContext {
  path: undefined | string;
}

const RepositoryContext = createContext<RepositoryContext>({ path: undefined });
export default RepositoryContext;
