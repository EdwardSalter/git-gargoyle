/**
 * Creates a functional component wrapper over a passed Component that stops the given props from being passed to the underlying Component.
 */
import { omit } from 'lodash-es';
import React, { FunctionComponent } from 'react';

export function omitProps<
  TExtraProps extends Record<string, unknown>,
  TComponent
>(keys: Array<keyof TExtraProps>) {
  return (
    Component: React.ComponentType<Omit<TComponent, keyof TExtraProps>>
  ): FunctionComponent<TComponent & TExtraProps> => {
    const WrappedComponent = (props: TExtraProps & TComponent) => {
      const omittedProps = omit(props, keys);
      return React.createElement(Component, omittedProps);
    };

    const innerName = getDisplayName(Component);
    WrappedComponent.displayName = `OmitProps(${innerName})`;

    return WrappedComponent;
  };
}

function getDisplayName<T>(Component: React.ComponentType<T>) {
  return Component.displayName || Component.name || 'Component';
}
