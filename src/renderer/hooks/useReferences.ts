import { useCallback, useEffect, useState } from 'react';
import { FromRenderer } from '../../constants/ipc';
import { Reference } from '../../main/git';

export interface ReferencesState {
  localBranches: Reference[];
  remoteBranches: Reference[];
  tags: Reference[];
  loading: boolean;
  error?: Error;
}

type ReturnType = [ReferencesState, () => Promise<void>];

export default function useReferences(path: string): ReturnType {
  const [references, setReferences] = useState<ReferencesState>({
    localBranches: [],
    tags: [],
    remoteBranches: [],
    loading: false,
  });

  const getReferences = useCallback(async () => {
    setReferences((prevState) => ({ ...prevState, loading: true }));
    try {
      const allRefs = await window.api.invoke(
        FromRenderer.GIT_BRANCH_START,
        path
      );

      setReferences({
        localBranches: allRefs.filter(
          (ref: Reference) => ref.type === 'branch'
        ),
        remoteBranches: allRefs.filter(
          (ref: Reference) => ref.type === 'remote'
        ),
        tags: allRefs.filter((ref: Reference) => ref.type === 'tag'),
        loading: false,
      });
    } catch (e) {
      setReferences((prevState) => ({
        ...prevState,
        loading: false,
        error: e,
      }));
    }
  }, [path]);

  useEffect(() => {
    // noinspection JSIgnoredPromiseFromCall
    getReferences();
  }, [getReferences]);

  return [references, getReferences];
}
