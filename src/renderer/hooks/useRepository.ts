import { useContext } from 'react';
import RepositoryContext from '../context/repository';

export default function useRepository() {
  return useContext(RepositoryContext);
}
