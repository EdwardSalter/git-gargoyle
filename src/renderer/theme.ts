import {
  accordionClasses,
  accordionSummaryClasses,
  createTheme,
  listItemClasses,
} from '@mui/material';
import { alpha } from '@mui/system';
import createPalette from '@mui/material/styles/createPalette';

const darkPalette = createPalette({
  mode: 'dark',
  grey: {
    '800': '#353535',
  },
});

const palette = createPalette({
  ...darkPalette,
  background: {
    paper: darkPalette.grey['800'],
  },
});
const theme = createTheme({
  palette,
  components: {
    MuiAccordion: {
      styleOverrides: {
        root: {
          border: `1px solid ${palette.divider}`,
          boxShadow: 'none',
          '&:not(:last-child)': {
            borderBottom: 0,
          },
          '&:before': {
            display: 'none',
          },

          [`&.${accordionClasses.expanded}`]: {
            margin: 0,
          },
        },
      },
    },

    MuiAccordionDetails: {
      styleOverrides: {
        root: {
          padding: 0,
        },
      },
    },

    MuiAccordionSummary: {
      styleOverrides: {
        root: {
          marginBottom: -1,
          background: palette.grey['900'],
          borderBottom: `1px solid ${palette.divider}`,
          minHeight: 48,

          [`&.${accordionSummaryClasses.expanded}`]: {
            minHeight: 48,
          },
        },
        content: {
          [`&.${accordionSummaryClasses.expanded}`]: {
            margin: 0,
          },
        },
      },
    },

    MuiListItem: {
      styleOverrides: {
        root: ({ theme }) => ({
          [`&.${listItemClasses.selected}`]: {
            background: alpha(theme.palette.primary.main, 0.2),
          },
        }),
      },
    },
  },
});

declare global {
  interface Window {
    theme: typeof theme;
  }
}

window.theme = theme;

export default theme;
