import { useEffect, useMemo, useState } from 'react';
import { FromMain, FromRenderer } from '../constants/ipc';

type Folder = string | null;

function triggerFolderChoiceDialog() {
  window.api.send(FromRenderer.SELECT_FOLDER_START);
}

function useFolderChoiceDialog(): [Folder, typeof triggerFolderChoiceDialog] {
  const [folder, setFolder] = useState<Folder>(null);
  const returning: [Folder, typeof triggerFolderChoiceDialog] = useMemo(() => {
    return [folder, triggerFolderChoiceDialog];
  }, [folder]);

  useEffect(() => {
    window.api.on(FromMain.SELECT_FOLDER_FINISH, setFolder);

    return () => {
      window.api.off(FromMain.SELECT_FOLDER_FINISH, setFolder);
    };
  }, []);

  return returning;
}

export default useFolderChoiceDialog;
